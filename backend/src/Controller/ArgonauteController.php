<?php

namespace App\Controller;

use App\Entity\Argonaute;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ArgonauteController extends AbstractController
{
    #[Route('/argonaute', name: 'app_index')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Argonaute::class);
        $argonautes = $repository->findAll();

        $payload = [];
        foreach($argonautes as $argonaute) {
            array_push($payload, $argonaute->getName());
        }

        return new JsonResponse([
            'message' => 'Welcome to your new controller!',
            'argonautes' => $payload,
        ]);
    }

    #[Route('/argonaute/add', name: 'app_add_argonaute', methods: ['POST'])]
    public function addArgonaute(Request $request, ManagerRegistry $doctrine, ValidatorInterface $validator): Response
    {
        $postData = $request->toArray();

        $vip = new Argonaute();
        $vip->setName($postData['name']);

        $errors = $validator->validate($vip);
        if (count($errors) > 0) {

            $payload = [];
            foreach($errors as $error) {
                array_push($payload, $error->getMessage());
            }

            return new JsonResponse([
                'message' => 'Validation Error',
                'errors' => $payload,
            ], 400);
        }

        $em = $doctrine->getManager();
        $em->persist($vip);
        $em->flush();

        return new JsonResponse([
            'message' => 'Welcome to app_add_argonaute!',
            'name' => $postData['name'],
        ]);
    }

    #[Route('/argonaute/{name}', name: 'app_remove_argonaute', methods: ['DELETE'])]
    public function removeArgonaute(string $name, Request $request, ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Argonaute::class);
        $decoded = base64_decode($name);
        $vipToDelete = $repository->findBy([ 'name' => $decoded ]);

        $em = $doctrine->getManager();
        foreach ($vipToDelete as $vip) {
            $em->remove($vip);
        }
        $em->flush();

        return new JsonResponse([
            'message' => 'Welcome to app_remove_argonaute!',
            'name' => $name,
        ]);
    }
}
