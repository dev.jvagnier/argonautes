// src/ArgonauteComponent.js

import _ from "lodash";
import { createRef, Fragment, useState } from "react";
import { Helmet } from "react-helmet-async";
import { toast } from "react-toastify";
import AddArgonauteComponent from "./AddArgonauteComponent";
import { addArgonaute, getArgonaute, removeArgonaute } from "./ArgonauteData";

const ArgonauteComponent = () => {
  const initState = {
    status: "idle",
    data: [],
  };

  // Créé l'état principal du composant à partir de l'objet init
  const [state, setState] = useState(initState);

  const argonauteRef = createRef();
  argonauteRef.current = {
    inputRef: createRef(),
    buttonRef: createRef(),
  };

  const resetData = () => setState(initState);

  const handleButton = (e) => {
    removeArgonaute(e.target.innerHTML)
      .then(resetData);
  };

  const toastError = (message) => {
    const { inputRef, buttonRef } = argonauteRef.current;
    toast(message, {
      onOpen: () => {
        inputRef.current.className = "input is-small is-rounded is-danger has-background-danger-light";
        buttonRef.current.className = "button is-small is-rounded is-danger";
      },
      onClose: () => {
        inputRef.current.className = "input is-small is-rounded";
        buttonRef.current.className = "button is-small is-rounded is-success";
      },
    });
  };

  const handleAddArgonaute = (e) => {
    e.preventDefault();

    const { inputRef } = argonauteRef.current;
    const value = inputRef.current.value;

    if (!value || value === "") {
      toastError("Oups ! Impossible d'enregistrer une valeur vide.");
    }
    else {
      addArgonaute(inputRef.current.value)
        .then(({ data, headers, status, statusText }) => {
          if (status >= 400) {
            _.each(data.errors, (error) => {
              toastError(error);
            });
          } else {
            const newState = _.assign({}, state);
            newState.data.push(data.name);
            setState(newState);
          }
        });
    };
  }

  if (state.status === "idle") {
    getArgonaute()
      .then((argonautes) => {
        const newState = _.assign({}, state);
        newState.data = argonautes;
        newState.status = "finished";
        setState(newState);
      });
  }

  if (state.status === "idle") return <p>En cours de chargement...</p>;
  if (!state.data.length === 0) return <p>Erreur de chargement !</p>;

  return (
    <Fragment>
      <Helmet>
        <title>Les Argonautes embarquent...</title>
      </Helmet>
      <div className="hero is-medium has-background-light">
        <div className="hero-body is-flex is-flex-direction-column is-align-items-center">
          <div className="image is-128x128">
            <img src="https://www.wildcodeschool.com/assets/logo_main-e4f3f744c8e717f1b7df3858dce55a86c63d4766d5d9a7f454250145f097c2fe.png" alt="Wild Code School logo" />
          </div>
          <h1 className="title">
            Les Argonautes
          </h1>
        </div>
      </div>
      <div className="section is-small has-background-dark is-flex is-flex-direction-column is-align-items-center">
        <p className="subtitle has-text-light">
          Ajouter un(e) Argonaute
        </p>
        <AddArgonauteComponent handler={handleAddArgonaute} ref={argonauteRef} />
      </div>
      <div className="section is-medium has-background-white is-flex is-flex-direction-column is-align-items-center">
        <p className="subtitle has-text-dark">
          Membres de l'équipage
        </p>
        <div className="field is-grouped is-grouped-multiline is-justify-content-center">
          {_.map(state.data, (argonaute, index) => {
            return (
              <div key={index} className="control">
                <button className="button is-danger is-rounded" onClick={handleButton}>
                  {argonaute}
                </button>
              </div>
            );
          })}
        </div>
      </div>
      <footer className="section is-small">
        <p className="has-text-centered has-text-light">Réalisé par Jason en Anthestérion de l'an 515 avant JC</p>
      </footer>
    </Fragment>
  );
}

export default ArgonauteComponent;
