// src/Argonautedata.js

import axios from "axios";
import _ from "lodash";

const API_URL = "/api"

export const getArgonaute = async () => {
  const argonautes = [];

  await axios.get(`${API_URL}/argonaute`)
    .then((response) => {
      _.map(response.data.argonautes, (argonaute) => argonautes.push(argonaute));
    });

  return argonautes;
};

export const addArgonaute = async (name) => {
  return await axios.post(`${API_URL}/argonaute/add`, { name: name })
    .then((response) => {
      return _.assign({}, response);
    })
    .catch((error) => {
      return _.assign({}, error.response);
    });
};

export const removeArgonaute = async (name) => {
  let result;

  await axios.delete(`${API_URL}/argonaute/${btoa(name)}`)
    .then((response) => {
      result = response.data.message;
    });

  return result;
};