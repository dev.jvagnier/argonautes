import './App.scss'

import React from 'react';
import { BrowserRouter, Routes, Route  } from "react-router-dom";
import { HelmetProvider } from 'react-helmet-async';
import ArgonauteComponent from './ArgonauteComponent';


export default function App() {
  return (
    <HelmetProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ArgonauteComponent />} />
        </Routes>
      </BrowserRouter>
    </HelmetProvider>
  );
}